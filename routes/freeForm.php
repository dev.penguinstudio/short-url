<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ShortUrlController;

Route::any('/{any}', [ShortUrlController::class, 'handleURL'])->where('any', '.*');