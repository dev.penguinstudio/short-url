<?php

use App\Http\Controllers\API\ManageURLController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::any('short-url', [ManageURLController::class, 'index']);
Route::get('short-url/{uuid}', [ManageURLController::class, 'details']);
// Route::any('test-api', function(Request $request) {
//     return response()->json($request->all());
// });