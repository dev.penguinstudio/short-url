<?php

use App\Http\Controllers\ApiLoggerController;
use App\Http\Controllers\ApiModelController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ShortUrlController;
use App\Http\Controllers\SubscriptionController;
use Illuminate\Support\Facades\Route;

Route::group(['as' => 'profile.', 'prefix' => 'profile'], function() {
    Route::get('/', [ProfileController::class, 'index'])->name('view');
});

Route::group(['as' => 'short-url.', 'prefix' => 'short-url'], function() {
    Route::get('/', [ShortUrlController::class, 'index'])->name('view');
    Route::post('create', [ShortUrlController::class, 'store'])->name('store');
    Route::post('validateCompleteURL', [ShortUrlController::class, 'validateCompleteURL'])->name('validateCompleteURL');
    // Route::post('update', [ShortUrlController::class, 'update'])->name('update');
    // Route::post('delete', [ShortUrlController::class, 'destroy'])->name('destroy');
    Route::get('viewShortURL', [ShortUrlController::class, 'viewShortURL'])->name('viewShortURL');
});

Route::group(['as' => 'api-doc.', 'prefix' => 'api-doc'], function() {
    Route::get('/', [ApiModelController::class, 'index'])->name('view');
    Route::post('store', [ApiModelController::class, 'store'])->name('store');
});

Route::get('apiLogs', [ApiLoggerController::class, 'show'])->name('api-logs');
Route::post('logout', [ProfileController::class, 'logOut'])->name('logout');
// Route::get('viewSubscription', [SubscriptionController::class, 'index'])->name('subscription.view');