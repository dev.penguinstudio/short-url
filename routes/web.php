<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\WebHomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [WebHomeController::class, 'index'])->name('web-home');

Route::get('authentication', [LoginController::class, 'showLoginForm'])->name('showLogin');
Route::get('login', [LoginController::class, 'showLoginForm']);
Route::get('register', [LoginController::class, 'showLoginForm']);
Route::post('login', [LoginController::class, 'login'])->name('login');
Route::post('requestOTP', [LoginController::class, 'requestOTP'])->name('requestOTP');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
// Route::get('getAllCustomers', [App\Http\Controllers\StripeController::class, 'getAllCustomers'])->name('getAllCustomers');
// Route::get('getCustomer/{customer_id}', [App\Http\Controllers\StripeController::class, 'getCustomer'])->name('getCustomer');