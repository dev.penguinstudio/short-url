<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::any('/v1/stripe_subscription', function(Request $request) {
    $data = response()->json([
        'method' => $_SERVER['REQUEST_METHOD'] ?? "DEFAULT_GET",
        'data' => $request->all()
    ]);
    Storage::put(uniqid(). ".json", $data);
});