{
    "openapi": "3.0.0",
    "info": {
        "title": "Fast Pigeon - Short URL API",
        "description": "API documentation for short url to access Fast Pigeon\\'s Short URL programatically.",
        "version": "1.0.1",
        "contact": {
            "name": "Fast Pigeon Support",
            "email": "support@fastpigeon.in"
        }
    },
    "paths": {
        "/api/short-url": {
            "get": {
                "tags": [
                    "short-url"
                ],
                "description": "Returns short urls",
                "parameters": [
                    {
                        "name": "api_key",
                        "in": "query",
                        "description": "It will be in Fast Pigeon Developer Portal.",
                        "required": true,
                        "schema": {
                            "type": "string"
                        }
                    },
                    {
                        "name": "page",
                        "in": "query",
                        "description": "It indicates the page which data is required.",
                        "required": false,
                        "schema": {
                            "type": "integer"
                        }
                    },
                    {
                        "name": "per_page",
                        "in": "query",
                        "description": "It indicates the number of data in a page.",
                        "required": false,
                        "schema": {
                            "type": "integer"
                        }
                    },
                    {
                        "name": "sort_by",
                        "in": "query",
                        "description": "It indicates the ordering of current data. Only available option is 'newest_first'",
                        "required": false,
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "A list of short urls.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "code": {
                                            "description": "Returns the status code of api response.",
                                            "type": "integer"
                                        }
                                    }
                                },
                                "examples": {
                                    "data": {
                                        "summary": "All Data",
                                        "value": {
                                            "code": 200,
                                            "message": "success",
                                            "data": {
                                                "page_data": [
                                                    {
                                                        "uuid": "surl-1693638792-64f2e0880a1de",
                                                        "short_url": "lZr4S/64f2e0880a16b",
                                                        "full_url": "https://www.bookmyshow.com",
                                                        "fallback_url": null,
                                                        "domain": null,
                                                        "status": 1
                                                    },
                                                    {
                                                        "uuid": "surl-1693638354-64f2ded2e36c5",
                                                        "short_url": "lZr4S/64f2ded2e36c1",
                                                        "full_url": "https://www.zee5.com",
                                                        "fallback_url": null,
                                                        "domain": null,
                                                        "status": 1
                                                    },
                                                    {
                                                        "uuid": "surl-1693616577-64f289c11fe93",
                                                        "short_url": "lZr4S/64f289c11fe8b",
                                                        "full_url": "https://www.aol.com",
                                                        "fallback_url": null,
                                                        "domain": null,
                                                        "status": 1
                                                    }
                                                ],
                                                "current_page": 1,
                                                "per_page": 3,
                                                "last_page": 7,
                                                "total_items": 21
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "post": {
                "tags": [
                    "short-url"
                ],
                "description": "Create new short url",
                "requestBody": {
                    "content": {
                        "application/json": {
                            "schema": {
                                "type": "object",
                                "properties": {
                                    "api_key": {
                                        "description": "API key will be obtained from Fast Pigeon's Customer Panel",
                                        "type": "string"
                                    },
                                    "full_url": {
                                        "description": "The complete URL which needs to be shorten.",
                                        "type": "string"
                                    },
                                    "fallback_url": {
                                        "description": "It indicates the back up URL if the full url can not be open.",
                                        "type": "string"
                                    },
                                    "required": [
                                        "api_key",
                                        "full_url"
                                    ]
                                }
                            },
                            "examples": {
                                "minimum_data": {
                                    "summary": "Create Short URL with minimum data",
                                    "value": {
                                        "api_key": "89a3c27933af0bbecc74f2581489eeaa",
                                        "full_url": "https://www.google.com"
                                    }
                                },
                                "all_data": {
                                    "summary": "Create Short URLs",
                                    "value": {
                                        "api_key": "89a3c27933af0bbecc74f2581489eeaa",
                                        "full_url": "https://www.google.com",
                                        "fallback_url": "https://www.google.co.in"
                                    }
                                }
                            }
                        }
                    }
                },
                "responses": {
                    "201": {
                        "description": "New Short URL is created.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "code": {
                                            "description": "Returns the status code of api response.",
                                            "type": "integer"
                                        }
                                    }
                                },
                                "examples": {
                                    "success": {
                                        "summary": "New Short URL is created.",
                                        "value": {
                                            "code": 201,
                                            "message": "success",
                                            "data": {
                                                "uuid": "surl-1693638792-64f2e0880a1de",
                                                "short_url": "lZr4S/64f2e0880a16b",
                                                "full_url": "https://www.google.com",
                                                "fallback_url": null
                                            }
                                        }
                                    },
                                    "failed_422": {
                                        "summary": "Error due to insufficient data or validation error.",
                                        "value": {
                                            "code": 422,
                                            "message": "This URL is already exists. You can not take this URL once again."
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "patch": {
                "tags": [
                    "short-url"
                ],
                "description": "Update existing short url",
                "requestBody": {
                    "content": {
                        "application/json": {
                            "schema": {
                                "type": "object",
                                "properties": {
                                    "api_key": {
                                        "description": "API key will be obtained from Fast Pigeon's Customer Panel",
                                        "type": "string"
                                    },
                                    "uuid": {
                                        "description": "UUID of short url.",
                                        "type": "string"
                                    },
                                    "full_url": {
                                        "description": "The complete URL which needs to be shorten.",
                                        "type": "string"
                                    },
                                    "fallback_url": {
                                        "description": "It indicates the back up URL if the full url can not be open.",
                                        "type": "string"
                                    },
                                    "required": [
                                        "api_key",
                                        "uuid",
                                        "full_url"
                                    ]
                                }
                            },
                            "examples": {
                                "minimum_data": {
                                    "summary": "Update Short URL with minimum data",
                                    "value": {
                                        "api_key": "89a3c27933af0bbecc74f2581489eeaa",
                                        "uuid": "surl-1693638792-64f2e0880a1de",
                                        "full_url": "https://www.google.com"
                                    }
                                },
                                "all_data": {
                                    "summary": "Update Short URL",
                                    "value": {
                                        "api_key": "89a3c27933af0bbecc74f2581489eeaa",
                                        "uuid": "surl-1693616577-64f289c11fe93",
                                        "full_url": "https://www.google.com",
                                        "fallback_url": "https://www.google.co.in"
                                    }
                                }
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "Short URL is updated.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "code": {
                                            "description": "Returns the status code of api response.",
                                            "type": "integer"
                                        }
                                    }
                                },
                                "examples": {
                                    "success": {
                                        "summary": "Short URL is updated.",
                                        "value": {
                                            "code": 200,
                                            "message": "success",
                                            "data": {
                                                "uuid": "surl-1693638792-64f2e0880a1de",
                                                "short_url": "lZr4S/64f47232a3d1b",
                                                "full_url": "https://www.google.com",
                                                "fallback_url": null,
                                                "domain": null,
                                                "status": 1
                                            }
                                        }
                                    },
                                    "failed_422": {
                                        "summary": "Error due to insufficient data or validation error.",
                                        "value": {
                                            "code": 422,
                                            "message": "The selected uuid is invalid."
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "delete": {
                "tags": [
                    "short-url"
                ],
                "description": "Delete existing short url",
                "requestBody": {
                    "content": {
                        "application/json": {
                            "schema": {
                                "type": "object",
                                "properties": {
                                    "api_key": {
                                        "description": "API key will be obtained from Fast Pigeon's Customer Panel",
                                        "type": "string"
                                    },
                                    "uuid": {
                                        "description": "UUID of short url.",
                                        "type": "string"
                                    },
                                    "required": [
                                        "api_key",
                                        "uuid"
                                    ]
                                }
                            },
                            "examples": {
                                "minimum_data": {
                                    "summary": "Delete Short URL",
                                    "value": {
                                        "api_key": "89a3c27933af0bbecc74f2581489eeaa",
                                        "uuid": "surl-1693638792-64f2e0880a1de"
                                    }
                                }
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "Short URL is deleted.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "code": {
                                            "description": "Returns the status code of api response.",
                                            "type": "integer"
                                        }
                                    }
                                },
                                "examples": {
                                    "success": {
                                        "summary": "Short URL is deleted.",
                                        "value": {
                                            "code": 200,
                                            "message": "succes"
                                        }
                                    },
                                    "failed_422": {
                                        "summary": "Error due to insufficient data or validation error.",
                                        "value": {
                                            "code": 422,
                                            "message": "The selected uuid is invalid."
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        "/api/short-url/{uuid}": {
            "get": {
                "tags": [
                    "short-url"
                ],
                "description": "Returns short urls",
                "parameters": [
                    {
                        "name": "api_key",
                        "in": "query",
                        "description": "It will be in Fast Pigeon Developer Portal.",
                        "required": true,
                        "schema": {
                            "type": "string"
                        }
                    },
                    {
                        "name": "uuid",
                        "in": "path",
                        "description": "It indicates the uuid of short url.",
                        "required": true,
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Short URL details",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "code": {
                                            "description": "Returns the status code of api response.",
                                            "type": "integer"
                                        }
                                    }
                                },
                                "examples": {
                                    "data": {
                                        "summary": "Short URL details based on UUID.",
                                        "value": {
                                            "code": 200,
                                            "message": "success",
                                            "data": {
                                                "uuid": "surl-1693638792-64f2e0880a1de",
                                                "short_url": "lZr4S/64f2e0880a16b",
                                                "full_url": "https://www.bookmyshow.com",
                                                "fallback_url": null,
                                                "domain": null,
                                                "status": 1
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    },
    "components": {}
}