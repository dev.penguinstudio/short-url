@extends('layouts.app', ['page_title' => 'Dashboard'])
@section('content')

<div class="row">
    <div class="col-lg-4 col-6">
        <div class="small-box bg-gradient-info">
            <div class="inner">
                <h3>{{ $data['links'] }}</h3>
                <p>Short Links</p>
            </div>

            <div class="icon">
                <i class="ti ti-link"></i>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-6">
        <div class="small-box bg-gradient-danger">
            <div class="inner">
                <h3>{{ $data['redirects'] }}</h3>
                <p>Redirects</p>
            </div>

            <div class="icon">
                <i class="zmdi zmdi-arrow-split"></i>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-6">
        <div class="small-box bg-gradient-success">
            <div class="inner">
                <h3>{{ $data['api_logs'] }}</h3>
                <p>API Requests</p>
            </div>

            <div class="icon">
                <i class="zmdi zmdi-code"></i>
            </div>
        </div>
    </div>

    {{-- <div class="col-lg-3 col-6">
        <div class="small-box bg-gradient-indigo">
            <div class="inner">
                <h3>{{ $data['clicks'] }}</h3>
                <p>Clicks</p>
            </div>

            <div class="icon">
                <i class="ti ti-hand-point-up"></i>
            </div>
        </div>
    </div> --}}
</div>

<div class="row">
    <div class="col-md-8 col-sm-12">
        <table class="table" {{ count($data['last_five_clicks'] ?? []) == 0 ? 'hidden' : '' }}>
            <thead>
                <tr>
                    <th style="border-color: transparent">Last 5 redirects</th>
                </tr>
            </thead>
            <tbody>
                @foreach (($data['last_five_clicks'] ?? []) as $item)
                <tr>
                    <td style="border-color: transparent">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5><a class="text-warning" href="{{ url($item->short_url) }}">{{ $item->short_url }}</a></h5>
                                    </div>
                                </div>
    
                                <div class="row mt-2">
                                    <div class="col-md-6">
                                        <label for="">
                                            <i class="icon icon-globe"></i>
                                        </label>
                                        <span class="text-bold">{{ $item->ip }}</span>
                                        @if ($item->ip_details->org)
                                        (<span class="text-medium">{{ $item->ip_details->org ?? 'N/A' }}</span>)
                                        @endif
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <label for="">
                                            <i class="ti ti-location-pin" aria-hidden="true"></i>
                                        </label>
                                        <span class="text-medium">{{ $item->ip_details->full_region }}</span>
                                    </div>
                                </div>
    
                                <div class="row mt-0">
                                    <div class="col-md-6 col-sm-12">
                                        <label for="">
                                            @if (Str::lower($item->device_details->device_type ?? "") != 'mobile')
                                            <i class="zmdi zmdi-laptop"></i>
                                            @else
                                            <i class="ti ti-mobile"></i>
                                            @endif
                                        </label>
                                        <span class="text-medium">{{ $item->device_details->browser ?? "N/A" }}</span>
                                        @if ($item->device_details->browser_version ?? "")
                                        [<span class="text-medium">V{{ $item->device_details->browser_version ?? 'N/A' }}</span>]
                                        @endif
                                        [{{ $item->device_details->device_family ?? 'N/A' }}]
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <label for="">
                                            <i class="icon icon-clock"></i>
                                        </label>
                                        <span class="text-medium">
                                            {{ $item->updated_at }}&nbsp;UTC
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endSection