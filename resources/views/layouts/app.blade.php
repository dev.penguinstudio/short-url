<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.header')
</head>

<body class="hold-transition dark-mode sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
    <div class="wrapper">
        <nav class="main-header navbar navbar-expand navbar-dark">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
            </ul>

            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"
                        role="button" onclick="logoutPopup()">
                        <i class="zmdi zmdi-power"></i>
                    </a>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
						@csrf
					</form>
                </li>
            </ul>
        </nav>

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <a href="{{ route('web-home') }}" class="brand-link">
                <img src="{{ asset('/resources/img/logo.png') }}" alt="" class="brand-image"/>
					<span class="brand-text font-weight-light text">
						<span class="">
                            {{ config('app.name') }}
                        </span>
					</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="{{ asset('/resources/img/user.png') }}" class="img-circle elevation-2" alt="" />
                    </div>
                    <div class="info">
                        <a href="" class="d-block">Howdy  {{ auth()->user()->first_name }}</a>
                    </div>
                </div>


                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">
                        <li class="nav-item">
                            <a href="{{ route('home') }}" class="nav-link  {{ Route::is('home') ? 'active' : '' }}">
                                <i class="nav-icon zmdi zmdi-view-dashboard"></i>
                                <p>Dashboard</p>
                            </a>
                        </li>

						<li class="nav-item">
                            <a href="{{ route('short-url.view') }}" class="nav-link {{ Route::is('short-url.*') ? 'active' : '' }}">
                                <i class="nav-icon zmdi zmdi-link"></i>
                                <p>Manage Short URL</p>
                            </a>
                        </li>

						<li class="nav-item">
                            <a href="{{ route('api-doc.view') }}" class="nav-link {{ Route::is('api-doc.*') ? 'active' : '' }}">
                                <i class="nav-icon zmdi zmdi-code"></i>
                                <p>API Documentation</p>
                            </a>
                        </li>
						<li class="nav-item">
                            <a href="{{ route('api-logs') }}" class="nav-link {{ Route::is('api-logs') ? 'active' : '' }}">
                                <i class="nav-icon zmdi zmdi-file-text"></i>
                                <p>API Logs</p>
                            </a>
                        </li>

                        {{-- <li class="nav-item">
                            <a href="{{ route('subscription.view') }}" class="nav-link {{ Route::is('subscription.view') ? 'active' : '' }}">
                                <i class="nav-icon fa fa-usd"></i>
                                <p>My Subscription</p>
                            </a>
                        </li> --}}
                        
                        @if (auth()->user()->is_admin)
                        <li class="nav-item">
                            <a href="{{ route('users.view') }}" class="nav-link">
                                <i class="nav-icon fa fa-users"></i>
                                <p>Manage Users</p>
                            </a>
                        </li>
                        @endif
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
			<div class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-12">
							<h3 class="m-0">{{ $page_title ?? "" }}</h3>
						</div>
					</div>
				</div>
			</div>

			<section class="content">
				<div class="container-fluid">
					@yield('content')
				</div>
			</section>
        </div>
        <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <footer class="main-footer">
            Powered by&nbsp;
            <strong class="font-pacifico">Fast Pigeon</strong>&nbsp;Enterprise
            <div class="float-right d-none d-sm-inline-block">
                <b>Version</b> 1.0.1
            </div>
        </footer>
    </div>
    <!-- ./wrapper -->

    <script src="{{ asset('/resources/admin/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('/resources/admin/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <script src="{{ asset('/resources/admin/js/adminlte.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script>
        $(document).ready(function() {
            catchResponse()
        });

        function catchResponse() {
            var allErrors = ('{{ $errors->first() }}');
            if (allErrors) {
                showMessage("error", allErrors)
            }
            var allSuccess = ('{{ session('success') }}');
            if (allSuccess) {
                showMessage("success", allSuccess)
            }
        }

        function showMessage(type = "success", context) {
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": true,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            switch (type) {
                case "error":
                    toastr.error(context)
                    break;
                case "warning":
                    toastr.warning(context)
                    break;
                case "success":
                    toastr.success(context)
            }
        }

        function logoutPopup() {
            Swal.fire({
                title: 'Alert!',
                text: 'Are you sure you want to log out?',
                icon: 'question',
                showCancelButton: true,
                cancelButtonText: 'No',
                confirmButtonText: 'Yes',
                confirmButtonColor: '#d33724',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    document.getElementById('logout-form').submit();
                }
            });
        }
    </script>
</body>

</html>
