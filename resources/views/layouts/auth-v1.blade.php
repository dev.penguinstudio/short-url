<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.header')
</head>

<body class="hold-transition login-page dark-mode bg-gradient-dark">
    <div class="login-box">
        <div class="card card-outline card-success">
            <div class="card-header text-center">
                <a href="{{ route('web-home') }}" class="h1">
					{{ config('app.name') }}
				</a>
            </div>
            <div class="card-body">
                <p class="login-box-msg">Sign In / Sign Up to start session</p>

                <form method="POST" action="{{ route('login') }}" id="login-form">
					@csrf
					<input type="hidden" name="country_code" value="{{ $country }}">
					<input type="hidden" name="dial_code" value="" id="dial-code">
                    <div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text">
								<img 
									src="https://flagcdn.com/{{ Str::lower($country) }}.svg"
									alt="" 
									srcset=""
									width="24"
								>
							</span>
						</div>
                        <input 
							type="tel" 
							class="form-control" 
							placeholder="Mobile Number" 
							name="mobile_number" 
							required 
							id="mobile_number"
						>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-mobile-alt"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3" id="otp-textbox">
                        <input type="text" class="form-control" placeholder="One Time Password" maxlength="6" name="otp">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
					<div class="input-group mb-3" id="captcha-container">
						<div id="recaptcha-container"></div>
					</div>
                    <div class="row">
                        <div class="col-12">
							<button id="otp-button" type="button" class="btn btn-primary btn-block" onclick="requestOTP()">Get OTP</button>
                            <button id="submit-button" type="button" class="btn btn-primary btn-block" onclick="submitButtonClicked()">Sign In</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

	<form action="{{ route('requestOTP') }}" method="post" id="request-otp">
		@csrf
		<input type="hidden" name="mobile_number" id="otp-mobile" value="" required>
		<input type="hidden" name="dial_code" id="dial-code" value="" required>
	</form>
	<script src="{{ asset('/resources/admin/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('/resources/admin/js/adminlte.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
	<script src="https://www.gstatic.com/firebasejs/6.0.2/firebase.js"></script>
	
	<script>
		$(document).ready(function() {
		    catchResponse()
			const mobileNumber = '{{ session("mobile_number") }}';
			if(mobileNumber) {
				$('#mobile_number').val(mobileNumber);
				$("#mobile_number").prop("readonly", true);
				showOTPField();
				console.info(mobileNumber);
			} else {
				fetchCountryData('{{ $country }}');
				hideOTPField();
			}
		});

		function catchResponse() {
            var allErrors = ('{{ $errors->first() }}');
            if (allErrors) {
			    showMessage("error", allErrors)
            }
            var allSuccess = ('{{ session("success") }}');
            if (allSuccess) {
		    	showMessage("success", allSuccess)
            }
        }

		function showMessage(type="success", context) {
		    toastr.options = {
  		    	"closeButton": false,
  		    	"debug": false,
  		    	"newestOnTop": true,
  		    	"progressBar": false,
  		    	"positionClass": "toast-top-right",
  		    	"preventDuplicates": true,
  		    	"onclick": null,
  		    	"showDuration": "300",
  		    	"hideDuration": "1000",
  		    	"timeOut": "5000",
  		    	"extendedTimeOut": "1000",
  		    	"showEasing": "swing",
  		    	"hideEasing": "linear",
  		    	"showMethod": "fadeIn",
  		    	"hideMethod": "fadeOut"
		    }
		    switch (type) {
		    	case "error":
		    		toastr.error(context)
		    		break;
		    	case "warning":
		    		toastr.warning(context)
		    		break;
		    	case "success":
		    		toastr.success(context)
		    }
		}

		function submitButtonClicked() {
			const county = '{{ $country }}';
			if(county != "IN") {
				const otp = $('#otp').val();
				verifyOTP(otp);
			} else {
				fetchCountryDataAndGoForLogin(county);
			}
			
		}

		function requestOTP() {
			const mobileValue = $('#mobile_number').val();
			if(!mobileValue) {
				showMessage("error", "Please enter mobile number")
				return;
			}
			requestOTPForMobileNumber('{{ $country }}', mobileValue);
        }

		function hideOTPField() {
			$('#otp-button'). attr("disabled", true);
			const county = '{{ $country }}';
			if(county != "IN") {
				window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
					'recaptcha-container', 
					{
					'theme': 'dark',
					'callback': function (response) {
						captchaResponse = response;
						$('#otp-button'). attr("disabled", false);
					}
				});
            	recaptchaVerifier.render();
			} else {
				$('#otp-button'). attr("disabled", false);
			}
			$('#otp-textbox').css('display', 'none');
			$('#otp-button').css('display', 'block');
			$('#submit-button').css('display', 'none');
		}

		function showOTPField() {
			$('#captcha-container').css('display', 'none');
			$('#otp-button').css('display', 'none');
			$('#submit-button').css('display', 'block');
		}
	</script>
	@if ($country != "IN")
	<script src="{{ asset('/resources/admin/js/firebase-config.js') }}"></script>
	@endif
	
	<script src="{{ asset('/resources/admin/js/mobile-otp.js') }}"></script>
</body>

</html>
