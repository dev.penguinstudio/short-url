<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
@include('layouts.metamanager')
<title>
    {{ config('app.name') }} {{ ($page_title ?? "") != "" ? ("- " . $page_title) : ("- " . config('app.app_name')) }}
</title>
<!--favicon-->
<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/resources/img/logo.png') }}" />
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/resources/img/logo.png') }}" />
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/resources/img/logo.png') }}" />

<link href="{{ asset('/resources/admin/css/icons.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{ asset('/resources/admin/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('/resources/admin/plugins/fontawesome-free/css/all.css') }}" />
<link href="{{ asset('/resources/admin/css/style.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
<script src="https://code.jquery.com/jquery-3.7.0.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
