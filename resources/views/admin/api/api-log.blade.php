@extends('layouts.app', ['page_title' => 'API Logs'])
@section('content')
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css">
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body table-responsive">
                <table id="dt-table" class="table table-bordered table-striped text-nowrap">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Method</th>
                            <th>URI</th>
                            <th>Response Code</th>
                            <th>Message</th>
                            <th>Timestamp</th>
                        </tr>
                    </thead>
                    <tbody>
                        @for ($i=0; $i<count($data); $i++)
                        <tr>
                            <td>
                                {{ ($page - 1) * $perPage + $i+1 }}
                            </td>
                            <td>
                                @switch($data[$i]->method)
                                    @case("GET")
                                        <span class="badge badge-primary">{{ $data[$i]->method }}</span>
                                        @break
                                    @case("POST")
                                        <span class="badge badge-success">{{ $data[$i]->method }}</span>
                                        @break
                                    @case("PATCH")
                                        <span class="badge badge-warning">{{ $data[$i]->method }}</span>
                                        @break
                                    @case("DELETE")
                                        <span class="badge badge-danger">{{ $data[$i]->method }}</span>
                                        @break
                                    @default
                                        <span class="badge badge-secondary">{{ $data[$i]->method }}</span>
                                @endswitch
                            </td>
                            <td>
                                {{ $data[$i]->path }}
                            </td>
                            <td>
                                {{ $data[$i]->status }}
                            </td>
                            <td>
                                {{ $data[$i]->message }}
                            </td>
                            <td>
                                {{ $data[$i]->created_at }}
                                <span class="text-sm">UTC</span>
                            </td>
                        </tr>
                        @endfor
                    </tbody>
        
                    <tfoot class="text-right">
                        <tr>
                            <td colspan="6">
                                {!! $data->withQueryString()->links('pagination::bootstrap-5') !!}
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
<script>
    $('document').ready(function(){
	    $('#dt-table').DataTable({
		    "ordering": false,
            'paging': false,
            "bInfo" : false
	    });
    })
</script>
@endsection