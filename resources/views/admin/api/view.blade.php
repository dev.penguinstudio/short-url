@extends('layouts.app', ['page_title' => 'API Documentation'])
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header text-right">
                <div class="">
                    <a 
                            href="{{ url('api/documentation') }}" 
                            class="btn btn-app btn-outline-success"
                            target="_blank"
                            >
                                <i class="fa fa-eye"></i>
                                &nbsp;API Document
                        </a>
                        <a 
                            href="javascript:void(0)" 
                            class="btn btn-app btn-outline-success"
                            data-toggle="modal"
                            data-target="#create-short-url"
                            onclick="createAPIKey()"
                            >
                            <i class="fa fa-plus"></i>
                            &nbsp;Create API Key
                        </a>
                </div>
            </div>
            <div class="card-body table-responsive">
                <table class="table text-nowrap table-striped hover">
                    <thead>
                        <tr>
                            <th class="table-plus">#</th>
                            <th>API Key</th>
                            <th>Created Date</th>
                            <th class="text-right">Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @for ($i = 0; $i < count($data); $i++)
                            <tr>
                                <td>{{ $i+1 }}</td>
                                <td>{{ $data[$i]->api_key }}</td>
                                <td>
                                    {{ date_format(date_create($data[$i]->created_at), 'l M d y') }} <br>
                                    {{ date_format(date_create($data[$i]->created_at), 'h:i:s a') }}
                                </td>
                                <td class="text-right">
                                    <div class="btn-group">
                                        <button 
                                            type="button" 
                                            class="btn btn-default"
                                            >
                                            <i class="zmdi zmdi-refresh-alt"></i>
                                        </button>
                                        <button 
                                            type="button" 
                                            class="btn btn-danger"
                                            >
                                            <i class="icon icon-trash"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endfor
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

<form method="POST" action="{{ route('api-doc.store') }}" id="create-api-key">
    @csrf
</form>

<script>
    function createAPIKey() {
        $('#create-api-key').submit();
    }
</script>
@endsection