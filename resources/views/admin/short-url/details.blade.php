@extends('layouts.app', ['page_title' => 'View Short URL [' . ($short_url ?? "") . ']' ])
@section('content')
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css">

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body table-responsive">
                <table id="dt-table" class="table table-bordered table-striped text-nowrap">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Redirected URL</th>
                            <th>IP</th>
                            <th>Location</th>
                            <th>Device Details</th>
                            <th>Timestamp</th>
                            {{-- <th class="text-center">Action</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @for ($i=0; $i<count($tracker_data); $i++)
                        <tr>
                            <td>
                                {{ ($page - 1) * $perPage + $i+1 }}
                            </td>
                            <td>
                                <a class="text-white" href="{{ $tracker_data[$i]->redirected_url }}">{{ $tracker_data[$i]->redirected_url }}</a>
                            </td>
                            <td>
                                {{ $tracker_data[$i]->ip }}
                            </td>

                            <td>
                                {{ 
                                    $tracker_data[$i]->ip_details->full_region ?? "N/A"
                                }}
                            </td>
                            <td>
                                <label for="">
                                    @if (Str::lower($tracker_data[$i]->device_details->device_type ?? "") != 'mobile')
                                    <i class="zmdi zmdi-laptop"></i>
                                    @else
                                    <i class="ti ti-mobile"></i>
                                    @endif
                                </label>
                                <span class="text-medium">{{ $tracker_data[$i]->device_details->browser ?? "N/A" }}</span>
                                @if ($tracker_data[$i]->device_details->browser_version ?? "")
                                [<span class="text-medium">V{{ $tracker_data[$i]->device_details->browser_version ?? 'N/A' }}</span>]
                                @endif
                                [{{ $tracker_data[$i]->device_details->device_family ?? 'N/A' }}]
                            </td>
                            <td>
                                {{ $tracker_data[$i]->created_at }}
                                <span class="text-sm">UTC</span>
                            </td>
                        </tr>
                        @endfor
                    </tbody>
                    <tfoot class="text-right">
                        <tr>
                            <td colspan="6" class="text-white">
                                {!! $tracker_data->withQueryString()->links('pagination::bootstrap-5') !!}
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
<script>
    $('document').ready(function(){
	    $('#dt-table').DataTable({
		    "ordering": false,
            'paging': false,
            "bInfo" : false
	    });
    })
</script>
@endsection