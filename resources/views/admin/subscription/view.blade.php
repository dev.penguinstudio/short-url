@extends('layouts.app', ['page_title' => 'Manage Subscription'])
@section('content')
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css">

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <p class="text-danger">
                    If you have already taken subscription, please visit
                    <a target="_blank" href="https://billing.stripe.com/p/login/test_00gg0CblU9wJ9So9AA"> Customer Subscrition Panel</a>
                </p>
            </div>
            <div class="card-body">
                <div>
                    <script async src="https://js.stripe.com/v3/pricing-table.js"></script>
                    <stripe-pricing-table pricing-table-id="{{ env('APP_DEBUG') ? config('stripe.local.price_table_id') : config('stripe.production.price_table_id') }}"
                        publishable-key="{{ env('APP_DEBUG') ? config('stripe.local.publishable_key') : config('stripe.production.publishable_key') }}">
                    </stripe-pricing-table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('document').ready(function(){
	    
    })
</script>
@endsection