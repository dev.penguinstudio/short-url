@extends('layouts.app', ['page_title' => 'Manage Short URL'])
@section('content')
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css">

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header text-right">
                <a 
                    href="javascript:void(0)" 
                    class="btn btn-outline-success"
                    data-toggle="modal"
					data-target="#create-short-url"
                    onclick="createURLAction()"
                    >
                    <i class="fa fa-plus"></i>
                    &nbsp;Create User
                </a>
            </div>
            <div class="card-body table-responsive">
                <table id="dt-table" class="table table-bordered table-striped text-nowrap">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>User Name</th>
                            <th>Email</th>
                            <th>Mobile Number</th>
                            <th>Country</th>
                            <th>Role</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @for ($i=0; $i<count($data); $i++)
                        <tr>
                            <td>
                                {{ ($page - 1) * $perPage + $i+1 }}
                            </td>
                            <td>
                                {{ $data[$i]->user_name }}
                            </td>
                            <td>
                                <a class="text-white" href="mailto:{{ $data[$i]->email }}">{{ $data[$i]->email }}</a>
                            </td>
                            <td>
                                {{ implode('-', [$data[$i]->dial_code, $data[$i]->mobile_number]) }}
                            </td>
                            <td>
                                {{ $data[$i]->country_code }}
                            </td>
                            <td>
                                {{ $data[$i]->role_name }}
                            </td>
                            <td class="text-right">
                                <div class="btn-group">
                                    <button 
                                        type="button" 
                                        class="btn btn-default"
                                        onclick="viewShortURL({{ $data[$i] }})"
                                        >
                                        <i class="zmdi zmdi-eye"></i>
                                    </button>
                                    <button 
                                        type="button" 
                                        class="btn btn-danger" 
                                        onclick="deleteShortURL('{{ $data[$i]->uuid }}')"
                                    >
                                        <i class="icon icon-trash"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                        @endfor
                    </tbody>
                    <tfoot class="text-right">
                        <tr>
                            <td colspan="4" class="text-white">
                                {!! $data->withQueryString()->links('pagination::bootstrap-5') !!}
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<div
	class="modal fade bs-example-modal-lg"
	id="create-short-url"
	tabindex="-1"
	role="dialog"
	aria-labelledby="createShortURL"
	aria-hidden="true">
	<div class="modal-dialog modal-md modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myLargeModalLabel">
                    <span id="form-name"></span>
				</h4>
				<button
					type="button"
					class="close"
					data-dismiss="modal"
					aria-hidden="true"
				>
                    <i class="ti ti-close"></i>
				</button>
			</div>
			<div class="modal-body">
                {{-- <div class="input-group mb-3">
                    <select name="domain" id="surl-domain" class="form-control">
                        <option value="">-- Select Domain --</option>
                        <option 
                            value="{{ env('APP_DEBUG') ? '$BASE_URL$' : 'shortmyurl.in' }}" selected
                            >
                            shortmyurl.in
                        </option>
                        <option value="f-url.in">f-url.in</option>
                        <option value="fply.in">fply.in</option>
                    </select>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="zmdi zmdi-globe"></span>
                        </div>
                    </div>
                </div> --}}
				<div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            {{-- {{ $base_path->base_path ?? "fp" }} --}}
                        </span>
                    </div>
                    <input
                        id="s-url"
                        type="text"
                        class="form-control"
                        placeholder="Enter Short URL"
                        required
                    />
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="zmdi zmdi-link"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input
                        id="f-url"
                        type="url"
                        class="form-control"
                        placeholder="Enter Complete URL"
                        required
                    />
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="zmdi zmdi-link"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input
                        id="fallback"
                        type="url"
                        class="form-control"
                        placeholder="Enter Fallback URL"
                    />
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="zmdi zmdi-link"></span>
                        </div>
                    </div>
                </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline-success" onclick="submitButtonAction()">
					Submit
				</button>
			</div>
		</div>
	</div>
</div>

<form action="" method="post" id="short-url-form">
    @csrf
    <input type="hidden" name="uuid" id="surl-uuid" value="">
    <input type="hidden" name="short_url" id="short-url">
    <input type="hidden" name="short_url_domain" id="short-url-domain">
    <input type="hidden" name="full_url" id="full-url">
    <input type="hidden" name="fallback_url" id="fallback-url">
</form>

{{-- <form action="{{ route('short-url.destroy') }}" method="POST" id="delete-short-url">
    @csrf
    <input type="hidden" name="uuid" id="surl-uuid-delete" value="">
</form> --}}

<form action="{{ route('short-url.viewShortURL') }}" method="GET" id="view-short-url">
    @csrf
    <input type="hidden" name="uuid" id="view-short-url-uuid" value="">
    <input type="hidden" name="short_url" id="view-short-url-short-url" value="">
</form>

<script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
<script>
    $('document').ready(function(){
	    $('#dt-table').DataTable({
		    "ordering": false,
            'paging': false,
            "bInfo" : false
	    });
    })

    function submitButtonAction() {
        if(!$('#f-url').val()) {
            showMessage('error', "Please enter complete url to create short url.");
            return;
        }
        validateCompleteURL();
    }

    function validateCompleteURL() {
        fetch(`{{ route('short-url.validateCompleteURL') }}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "X-Requested-With": "XMLHttpRequest",
                "X-CSRF-Token": $('input[name="_token"]').val()
            },
            credentials: "same-origin",
            body: JSON.stringify({
                'complete_url': $('#f-url').val(),
                'uuid': $('#surl-uuid').val() ?? ""
            })
        })
        .then(response => response.json())
        .then(data => {
            console.log('surl-uuid ', data);
            console.log('surl-uuid 2 ', $('#surl-uuid').val());
            if(data.code != 200) {
                showMessage('error', data.message);
            } else {
                if(!$('#surl-uuid').val()) {
                    proceedCreateShortURL();
                } else {
                    proceedUpdateShortURL();
                }
                
            }
        })
        .catch(error => console.error(error));
    }

    function proceedCreateShortURL() {
        $('#short-url').val($('#s-url').val());
        $('#full-url').val($('#f-url').val());
        $('#fallback-url').val($('#fallback').val());
        $('#short-url-domain').val($('#surl-domain').val());
        $('#short-url-form').attr('action', "{{ route('short-url.store') }}");
        $('#short-url-form').submit();
    }

    function copyToClipboard(id) {
        var textToCopy = document.getElementById(id).value;
        navigator.clipboard.writeText(textToCopy);
        showMessage('success', `${textToCopy} has been copied successfully.`)
    }

    function editUrl(shortURl) {
        $('#form-name').text("Edit Short URL");
        $('#surl-uuid').val(shortURl.uuid);
        $('#f-url').val(shortURl.full_url ?? "");
        $('#f-url').attr('readonly', true);
        $('#fallback').val(shortURl.fallback_url ?? "");
    }

    function proceedUpdateShortURL() {
        $('#short-url-domain').val($('#surl-domain').val());
        $('#short-url').val($('#s-url').val());
        $('#full-url').val($('#f-url').val());
        $('#fallback-url').val($('#fallback').val());
        $('#short-url-form').submit();
    }

    function createURLAction() {
        $('#f-url').attr('readonly', false);
        $('#form-name').text("Create Short URL");
        $('#surl-uuid').val("");
        $('#f-url').val("");
        $('#s-url').val("");
    }

    function deleteShortURL(uuid) {
        Swal.fire({
            title: 'Alert!',
            text: 'Are you sure you want to delete this short url?',
            icon: 'question',
            showCancelButton: true,
            cancelButtonText: 'No',
            confirmButtonText: 'Yes',
            confirmButtonColor: '#d33724',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $('#surl-uuid-delete').val(uuid);
                $('#delete-short-url').submit();
            }
        });
    }

    function viewShortURL(shortURL) {
        //view-short-url-short-url
        $('#view-short-url-uuid').val(shortURL.uuid);
        $('#view-short-url-short-url').val(shortURL.short_url);
        $('#view-short-url').submit();
    }
</script>
@endsection