<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author" content="Grayrids">
    @include('layouts.metamanager')
    <title>{{ config('app.name') }} &mdash; {{ config('app.app_name') }}</title>
    <link rel="shortcut icon" href="{{ asset('/resources/img/logo.png') }}" type="image/png">
    <link rel="stylesheet" href="{{ asset('/resources/web/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/resources/web/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('/resources/web/css/LineIcons.css') }}">
    <link rel="stylesheet" href="{{ asset('/resources/web/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('/resources/web/css/owl.theme.css') }}">
    <link rel="stylesheet" href="{{ asset('/resources/web/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('/resources/web/css/nivo-lightbox.css') }}">
    <link rel="stylesheet" href="{{ asset('/resources/web/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('/resources/web/css/responsive.css') }}">
</head>

<body>

    <header id="home" class="hero-area">
        <div class="overlay">
            <span></span>
            <span></span>
        </div>
        <nav class="navbar navbar-expand-md bg-inverse fixed-top scrolling-navbar">
            <div class="container">
                <a href="javascript:void(0)" class="navbar-brand">
                    <span style="font-family: 'Noto Sans'">Short My URL</span>
                </a>
                
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                    aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="lni-menu"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav mr-auto w-100 justify-content-end">
                        <li class="nav-item">
                            <a class="nav-link page-scroll" href="#home">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link page-scroll" href="#services">Features</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link page-scroll" href="#pricing">Pricing</a>
                        </li>
                        <li class="nav-item">
                            @auth
                                <a class="btn btn-singin" href="{{ route('home') }}">
                                    <span><i class="lni-bar-chart"></i></span>
                                    <span>My Dashboard</span>
                                </a>
                            @else
                                <a class="btn btn-singin" href="{{ route('showLogin') }}">
                                    <span><i class="lni-user"></i></span>
                                    <span>Sign In</span>
                                </a>
                            @endauth
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container">
            <div class="row space-100">
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="contents">
                        <h2 class="head-title">
                            Experience full control over your short links
                        </h2>
                        <p>
                            &#x25A1; Create short links and share them anywhere. <br>
                            &#x25A1; Track short url history. <br>
                            &#x25A1; Get clear vision from analytics.
                        </p>
                        <div class="header-button">
                            @auth
                            <a href="{{ route('home') }}" rel="nofollow" class="btn btn-border-filled">
                                Take me to Dashboard
                            </a>
                            @else
                            <a href="{{ route('showLogin') }}" rel="nofollow" class="btn btn-border-filled">
                                Lets Get Started
                            </a>
                            @endauth
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-xs-12 p-0">
                    <div class="intro-img">
                        <img src="{{ asset('/resources/img/analytics.svg') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </header>


    <section id="services" class="section">
        <div class="container">

            <div class="row">
                <div class="col-lg-4 col-md-6 col-xs-12">
                    <div class="services-item text-center">
                        <div class="icon">
                            <i class="lni-cog"></i>
                        </div>
                        <h4>Hassle Free Link management</h4>
                        <p class=" text-left">
                            &mdash; Fast Redirects <br>
                            &mdash; Custom URL slugs <br>
                            &mdash; URL Parameters
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-xs-12">
                    <div class="services-item text-center">
                        <div class="icon">
                            <i class="lni-stats-up"></i>
                        </div>
                        <h4>Rich Analytics</h4>
                        <p class="text-left">
                            &mdash; Link analytics <br>
                            &mdash; Link History <br>
                            &mdash; Link clicks<br>
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-xs-12">
                    <div class="services-item text-center">
                        <div class="icon">
                            <i class="lni-layers"></i>
                        </div>
                        <h4>Some more Features</h4>
                        <p class="text-left">
                            &mdash; Link to Bio <br>
                            &mdash; Fallback URL support<br>
                            &mdash; Developer API Support<br>
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section id="pricing" class="section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="pricing-text section-header text-center">
                        <div>
                            {{-- <h2 class="section-title">Our Pocket Friendly Plans</h2> --}}
                            <h2 class="section-title">Our Pricing</h2>
                            <div class="desc-text">
                                {{-- <p>Pricing for brands and businesses of all segments</p>
                                <p>No Yearly Commitments</p> --}}
                                <p>What is this? We don't require money at this time. Please feel free to use this product.</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            {{-- <div class="row pricing-tables">
                <div class="col-lg-4 col-md-4 col-xs-12">
                    <div class="pricing-table text-center">
                        <div class="pricing-details">
                            <h3>Free</h3>
                            <h1><span>$</span>0</h1>
                            <ul>
                                <li>
                                    10 Short Links
                                </li>
                                <li>
                                    Unlimited Redirection
                                </li>
                                <li>
                                    Basic Analytics
                                </li>
                                <li>
                                    <strike>Editable URL</strike>
                                </li>
                                <li>
                                    <strike>API Support</strike>
                                </li>
                                <li>
                                    <strike>Fallback URL Support</strike>
                                </li>
                            </ul>
                        </div>
                        <div class="plan-button">
                            <a href="{{ route('subscription.view') }}" class="btn btn-border">Subscribe</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-xs-12">
                    <div class="pricing-table text-center">
                        <div class="pricing-details">
                            <h3>standard</h3>
                            <h1><span>$</span>6.99</h1>
                            <ul>
                                <li>
                                    10 Short Links
                                </li>
                                <li>
                                    Unlimited Redirection
                                </li>
                                <li>
                                    Basic Analytics
                                </li>
                                <li>
                                    Editable URL
                                </li>
                                <li>
                                    <strike>API Support</strike>
                                </li>
                                <li>
                                    <strike>Fallback URL Support</strike>
                                </li>
                            </ul>
                        </div>
                        <div class="plan-button">
                            <a href="{{ route('subscription.view') }}" class="btn btn-common">Subscribe</a>
                        </div>
                    </div>
                </div>
               
                <div class="col-lg-4 col-md-4 col-xs-12">
                    <div class="pricing-table text-center">
                        <div class="pricing-details">
                            <h3>Business</h3>
                            <h1><span>$</span>9.99</h1>
                            <ul>
                                <li>
                                    Unlimited Short Links
                                </li>
                                <li>
                                    Unlimited Redirection
                                </li>
                                <li>
                                    Complete Analytics
                                </li>
                                <li>
                                    Editable URL
                                </li>
                                <li>
                                    API Support
                                </li>
                                <li>
                                    Fallback URL Support
                                </li>
                            </ul>
                        </div>
                        <div class="plan-button">
                            <a href="{{ route('subscription.view') }}" class="btn btn-border">Subscribe</a>
                        </div>
                    </div>
                </div>
            </div> --}}
        </div>
    </section>

    <footer>
        <section id="footer-Content">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
                        <div class="widget">
                            <h3 class="block-title">Industries</h3>
                            <ul class="menu">
                                <li><a href="#"> - Drop Shipping</a></li>
                                <li><a href="#">- Education</a></li>
                                <li><a href="#">- Banking</a></li>
                                <li><a href="#">- Automobile</a></li>
                                <li><a href="#">- ECommerce</a></li>
                                <li><a href="#">- Entertainment</a></li>
                                <li><a href="#">- Life Style</a></li>
                            </ul>
                        </div>
                    </div>
                    {{-- <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
                        <div class="widget">
                            <h3 class="block-title">Use Case</h3>
                            <ul class="menu">
                                <li><a href="#"> - About Us</a></li>
                                <li><a href="#">- Career</a></li>
                                <li><a href="#">- Blog</a></li>
                                <li><a href="#">- Press</a></li>
                            </ul>
                        </div>
                    </div> --}}
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
                        <div class="widget">
                            <h3 class="block-title">Important Links</h3>
                            <ul class="menu">
                                <li><a href="#">- Terms and Conditions</a></li>
                                <li><a href="#">- Privacy Policy</a></li>
                                <li><a href="#">- Cancellation Policy</a></li>
                                <li><a href="#">- Return & Refund Policy</a></li>
                                <li><a href="#">- Cookie Policy</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
                        <div class="widget">
                            <h3 class="block-title">Developers' Corner</h3>
                            <ul class="menu">
                                <li><a target="_blank" href="{{ url('api/documentation') }}"> - API Documentation</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
                        <div class="widget">
                            <h3 class="block-title">Contact Us</h3>
                            <ul class="menu">
                                <li>
                                    <p style="font-size: 20px">
                                        <span style="font-family: Pacifico; font-size: 20px">Fast Pigeon</span>
                                        Enterprise
                                    </p>
                                    <p>
                                        GSTIN: 21CKCPD1464M1ZG
                                    </p>
                                    <p>
                                        Address: 4660/4622, Matiasahi, Hakimpada, Angul, Odisha, 759143
                                    </p>
                                    <p>
                                        Email: support@fastpigeon.in
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    {{-- <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
                        <div class="widget">
                            <h3 class="block-title">Subscribe Now</h3>
                            <p>Will grateful if you subscribe to our NewsLetter and Marketing Updates</p>
                            <div class="subscribe-area">
                                <input type="email" class="form-control" placeholder="Enter Email">
                                <span><i class="lni-chevron-right"></i></span>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>

            <div class="copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="site-info text-center">
                                <p>
                                    Powered by 
                                    <span style="font-family: Pacifico">Fast Pigeon</span>&nbsp;Enterprise
                                </p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

    </footer>


    <a href="#" class="back-to-top">
        <i class="lni-chevron-up"></i>
    </a>
    
    <script src="{{ asset('/resources/web/js/jquery-min.js') }}"></script>
    <script src="{{ asset('/resources/web/js/popper.min.js') }}"></script>
    <script src="{{ asset('/resources/web/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/resources/web/js/owl.carousel.js') }}"></script>
    <script src="{{ asset('/resources/web/js/jquery.nav.js') }}"></script>
    <script src="{{ asset('/resources/web/js/scrolling-nav.js') }}"></script>
    <script src="{{ asset('/resources/web/js/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('/resources/web/js/nivo-lightbox.js') }}"></script>
    <script src="{{ asset('/resources/web/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('/resources/web/js/form-validator.min.js') }}"></script>
    <script src="{{ asset('/resources/web/js/contact-form-script.js') }}"></script>
    <script src="{{ asset('/resources/web/js/main.js') }}"></script>

</body>

</html>
