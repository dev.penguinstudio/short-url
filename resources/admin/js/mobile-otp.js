var countryDialCode = '';
var captchaResponse;

function fetchCountryData(country) {
    fetch('resources/others/country.json')
    .then((response) => response.json())
    .then((data) => {
        const searchIndex = data.findIndex((countryObject) => countryObject.code==country);
        if(searchIndex && data[searchIndex]) {
            countryDialCode = data[searchIndex].dial_code;
        }
    });
}
function isIndianMobileNumber(countryCode) {
    return (countryCode == 'IN');
}

function requestOTPForMobileNumber(countryCode, mobileNumber) {
    const isIndian = isIndianMobileNumber(countryCode);
    if(isIndian) {
        startIndianOTPFlow(mobileNumber);
        // startInternationlOTPFlow(mobileNumber)
    } else {
        startInternationlOTPFlow(mobileNumber)
    }
}

function startIndianOTPFlow(mobileNumber) {
    $('#otp-mobile').val(mobileNumber);
    $('#dial-code').val(countryDialCode);
    $('#request-otp').submit();
}

function startInternationlOTPFlow(mobileNumber) {
    sendFirebaseOTP(countryDialCode+mobileNumber);
}

function OTPSentSuccess(mobileNumber) {
    showOTPField();
    showMessage('success', "OTP has been sent to your mobile number.");
    $("#mobile_number").prop("readonly", true);
    $('#otp-textbox').css('display', 'block');
}

function OTPSentfailed(message) {
    hideOTPField();
    showMessage('error', message ?? "Something went wrong. Please try again!!");
}

function fetchCountryDataAndGoForLogin(country) {
    fetch('resources/others/country.json')
    .then((response) => response.json())
    .then((data) => {
        const searchIndex = data.findIndex((countryObject) => countryObject.code==country);
        if(searchIndex && data[searchIndex]) {
            countryDialCode = data[searchIndex].dial_code;
            $('#dial-code').val(countryDialCode);
            $('#login-form').submit();
        }
    });
}