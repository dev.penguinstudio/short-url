<?php

namespace App\Http\Utility;

use App\Models\IpDetails;
use App\Models\TrackClick;
use App\Models\UserAgent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Jenssegers\Agent\Agent;
use hisorange\BrowserDetect\Parser as Browser;

enum Tracker
{
    case CLICKS;
    case IP;
}

class TrackURL {
    
    private $trackers;
    private $minimum_ip_change_date = 7;
    public function __construct(array $trackers)
    {
        $this->trackers = $trackers;
    }

    public function track(
        mixed $short_url, 
        Request $request, 
        String $ip,
        String $final_url_to_redirect
    ) {
        foreach($this->trackers as $tracker) {
            switch($tracker) {
                case Tracker::CLICKS:
                    $this->trackClicks($short_url, $request, $ip, $final_url_to_redirect);
                    break;
                case Tracker::IP:
                    $this->trackIp($ip);
                    break;
                default:
                    break;
            }
        }
    }

    private function trackClicks(
        mixed $short_url, 
        Request $request, 
        String $ip,
        String $final_url_to_redirect
    ) {
        $new_clicks = new TrackClick();
        $new_clicks->uuid = uniqid('fp-click-' . time() . '-');
        $new_clicks->user_id = $short_url->user_id;
        $new_clicks->short_url = $short_url->short_url;
        $new_clicks->redirected_url = $final_url_to_redirect;
        $new_clicks->is_fallback = ($short_url->full_url != $final_url_to_redirect);
        $new_clicks->ip = $ip;
        $new_clicks->save();
        $this->trackDeviceDetails($new_clicks);
    }

    private function trackIp(
        String $ip
    ) {
        $ip_details = IpDetails::where('ip', $ip)->first();
        if($ip_details && ($ip_details->differenceTillDate <= $this->minimum_ip_change_date)) {
            return;
        }
        $response = Http::get('http://ip-api.com/json/'. $ip. '?fields=66846719');
        $jsonData = $response->json();
        if($jsonData && $jsonData['query'] && count($jsonData) >= 3) {
            $ip_details = IpDetails::where('ip', $ip)->first();
            if(!$ip_details) {
                $ip_details = new IpDetails();
                $ip_details->uuid = uniqid('fp-ip' . time() . '-');
                $ip_details->ip = $ip;
            }
            $ip_details->continent = $jsonData['continent'];
            $ip_details->continent_code = $jsonData['continentCode'];
            $ip_details->country = $jsonData['country'];
            $ip_details->country_code = $jsonData['countryCode'];
            $ip_details->region = $jsonData['region'];
            $ip_details->region_name = $jsonData['regionName'];
            $ip_details->city = $jsonData['city'];
            $ip_details->district = $jsonData['district'];
            $ip_details->zip = $jsonData['zip'];
            $ip_details->latitude = $jsonData['lat'];
            $ip_details->longitude = $jsonData['lon'];
            $ip_details->timezone = $jsonData['timezone'];
            $ip_details->offset = $jsonData['offset'];
            $ip_details->currency = $jsonData['currency'];
            $ip_details->isp = $jsonData['isp'];
            $ip_details->org = $jsonData['org'];
            $ip_details->as = $jsonData['as'];
            $ip_details->asname = $jsonData['asname'];
            $ip_details->reverse = $jsonData['reverse'];
            if($jsonData['mobile']) {
                $ip_details->connection_type = 'mobile';
            } elseif($jsonData['proxy']) {
                $ip_details->connection_type = 'proxy';
            } elseif($jsonData['hosting']) {
                $ip_details->connection_type = 'hosting';
            } else {
                $ip_details->connection_type = 'other';
            }
            $ip_details->save();
        }        
    }

    private function trackDeviceDetails(
        TrackClick $click
    ) {
        $agent = new Agent();
        $browser = $agent->browser();
        $platform = $agent->platform();
        $new_device = new UserAgent();
        $new_device->uuid = uniqid('fp-device-' . time() . '-');
        $new_device->track_id = $click->uuid;
        $new_device->user_id = $click->user_id;
        $new_device->short_url = $click->short_url;
        $new_device->device_type = Browser::deviceType();
        $new_device->device_family = Browser::deviceFamily();
        $new_device->platform = Browser::platformFamily();
        $new_device->platform_version = $agent->version($platform);
        $new_device->browser = Browser::browserFamily();
        $new_device->browser_version = $agent->version($browser);
        $new_device->is_inApp = Browser::isInApp();
        $new_device->save();
    }
}