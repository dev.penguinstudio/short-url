<?php

namespace App\Http\Controllers;

use App\Http\Utility\Tracker;
use App\Http\Utility\TrackURL;
use App\Models\BasePath;
use App\Models\ShortUrl;
use App\Models\TrackClick;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;

class ShortUrlController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $base_path = BasePath::where('user_id', auth()->user()->uuid)->first();
        $all_short_url = ShortUrl::where('user_id', auth()->user()->uuid)->orderBy('created_at', 'desc')->paginate(10);
        return view('admin.short-url.view')->with([
            'data' => $all_short_url,
            'page' => $all_short_url->currentPage(),
            'perPage' => $all_short_url->perPage(),
            'base_path' => $base_path
        ]);
    }

    public function viewShortURL(Request $request) {
        $request->validate([
            'short_url' => 'required|string|exists:short_urls',
            'uuid' => 'required|string|exists:short_urls'
        ]);

        $short_url = ShortUrl::where([
            'short_url' => $request->short_url,
            'uuid' => $request->uuid,
            'user_id' => auth()->user()->uuid
        ])->first();

        $tracker_data = TrackClick::where('short_url', $request->short_url)->orderBy('created_at', 'desc')->paginate(10);

        return view('admin.short-url.details')->with([
            'short_url' => $short_url->short_url,
            'data' => $short_url,
            'tracker_data' => $tracker_data,
            'page' => $tracker_data->currentPage(),
            'perPage' => $tracker_data->perPage(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'full_url' => 'required|string',
            'short_url' => 'nullable|string|max:10',
            'fallback_url' => 'nullable|string',
            'short_url_domain' => 'nullable|in:$BASE_URL$,shortmyurl.in,f-url.in,fply.in'
        ]);

        $short_url_count = count(ShortUrl::where([
            'full_url' => $request->complete_url,
            'user_id' => auth()->user()->uuid
        ])->get());
        if($short_url_count > 0) {
            return redirect()->back()->withErrors('This URL is already exists. You can not take this URL once again.');
        }

        $base_path = BasePath::where('user_id', auth()->user()->uuid)->first();
        $short_url = $request->short_url ?? uniqid();
        $new_short_url = new ShortUrl();
        $new_short_url->uuid = uniqid('surl-' . (int)time() . '-');
        $new_short_url->short_url = ($base_path->base_path ?? uniqid()) .'/'. $short_url;
        $new_short_url->full_url = $request->full_url;
        $new_short_url->fallback_url = $request->fallback_url;
        if($request->short_url_domain != '$BASE_URL$') {
            $new_short_url->domain = $request->short_url_domain;
        }
        // $new_short_url->domain = $request->domain ?? url();
        $new_short_url->user_id = auth()->user()->uuid;
        $new_short_url->save();
        return redirect()->back()->with('success', 'Short URL has been created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(ShortUrl $shortUrl)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ShortUrl $shortUrl)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, ShortUrl $shortUrl)
    {
        //
        $request->validate([
            'uuid' => 'required|string|exists:short_urls,uuid',
            'full_url' => 'required|string|exists:short_urls,full_url'
        ]);
        $base_path = BasePath::where('user_id', auth()->user()->uuid)->first();
        $short_url = $request->short_url ?? uniqid();
        
        $new_short_url = ShortUrl::where([
            'user_id' => auth()->user()->uuid,
            'uuid' => $request->uuid,
            'full_url' => $request->full_url,
        ])->first();
        if(!$new_short_url) {
            return redirect()->back()->withErrors('Unable to get the url. Please try again.');
        }
        $new_short_url->short_url = ($base_path->base_path ?? uniqid()) .'/'. $short_url;
        $new_short_url->fallback_url = $request->fallback_url;
        $new_short_url->user_id = auth()->user()->uuid;
        $new_short_url->save();
        return redirect()->back()->with('success', "Short URL has been updated successfully.");
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        //
        $request->validate([
            'uuid' => 'required|string|exists:short_urls,uuid'
        ]);
        $short_url = ShortUrl::where([
            'user_id' => auth()->user()->uuid,
            'uuid' => $request->uuid
        ])->first();
        if(!$short_url) {
            return redirect()->back()->withErrors('Unable to find short url.');
        }
        $short_url->delete();
        return redirect()->back()->with('success', 'Short URL has been deleted successfully.');
    }

    public function validateCompleteURL(Request $request) {
        $validateData = Validator::make($request->all(), [
            'complete_url' => 'required|string'
        ]);

        if($validateData->fails()) {
            return response()->json([
                'code' => 422,
                'message' => $validateData->errors()->first()
            ]);
        }

        if($request->uuid == "") {
            $short_url_count = count(ShortUrl::where([
                'full_url' => $request->complete_url,
                'user_id' => auth()->user()->uuid
            ])->get());
            if($short_url_count > 0) {
                return response()->json([
                    'code' => 423,
                    'message' => 'This URL is already exists. You can not take this URL once again.'
                ]);
            }
        }
        return response()->json([
            'code' => 200
        ]);
    }

    public function handleURL(Request $request) {
        $short_url_path = ltrim($request->getPathInfo(), '/');
        $short_url = ShortUrl::where('short_url', $short_url_path)->first();
        $default_domain = app('url')->to('/');
        if($short_url && $short_url->full_url) {
            // add domain based routing
            if($request->root() != ($short_url->domain ?? $default_domain)) {
                abort(401);
            }
            $ip = (env('APP_ENV') == 'local') ? file_get_contents('https://api.ipify.org'): $_SERVER["HTTP_CF_CONNECTING_IP"];
            if($request->wantsJson()) {
                return $this->sendJSONResponse($short_url, $request, $ip);
            } else {
                return $this->routeToNewPage($short_url, $request, $ip);
            }
        }
        abort(404);
    }
    
    private function sendJSONResponse(mixed $short_url, Request $request, String $ip = null) {
        $method_name = $request->getMethod();
        switch($method_name) {
            case "GET":
                $response = Http::withHeaders(
                    $request->header()
                )->get($short_url->full_url, $request->all());
                break;
            case "POST":
                $response = Http::withHeaders(
                    $request->header()
                )->post($short_url->full_url, $request->all());
                break;
            case "PUT":
                $response = Http::withHeaders(
                    $request->header()
                )->put($short_url->full_url, $request->all());
                break;
            case "PATCH":
                $response = Http::withHeaders(
                    $request->header()
                )->patch($short_url->full_url, $request->all());
                break;
            case "DELETE":
                $response = Http::withHeaders(
                    $request->header()
                )->delete($short_url->full_url, $request->all());
                break;
            default:
                return $this->routeToNewPage($short_url, $request, $ip); 
        }
        
        $statusCode = $response->status();
        try {
            $responseBody = json_decode($response->getBody(), true);
            return response()->json(
                data: $responseBody ?? file_get_contents($short_url->full_url), 
                status: $statusCode, 
                headers: [
                    'x-request-headers' => json_encode($request->header())
                ]
            );
        } catch (\Throwable $th) {
            return $this->routeToNewPage($short_url, $request, $ip); 
        }
        
    }

    private function routeToNewPage(mixed $short_url, Request $request, String $ip) {
        $final_url_to_redirect = $short_url->full_url;
        $validateData = Validator::make([
            'complete_url' => $short_url->full_url
        ], [
            'complete_url' => 'string'
        ]);
        if ($validateData->fails() && $short_url->fallback_url) {
            $final_url_to_redirect = $short_url->fallback_url;
        }
        if(count($request->all()) > 0) {
            $final_url_to_redirect = $final_url_to_redirect . '?' . Arr::query($request->all());
        }

        // TODO: Push it to Queue
        $tracker = new TrackURL([
            Tracker::CLICKS,
            Tracker::IP,
        ]);
        $tracker->track($short_url, $request, $ip, $final_url_to_redirect);
        $url = parse_url($final_url_to_redirect);
        if($url['scheme'] == 'https' || $url['scheme'] == 'http') {
            return redirect(
                to: $final_url_to_redirect,
                status: 302,
                headers: $request->header()
            );
        } else {
            return redirect()->away($final_url_to_redirect);
        }        
    }
}
