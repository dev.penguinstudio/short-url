<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Stripe\StripeClient;

class StripeController extends Controller
{
    //
    private $stripe;

    public function __construct()
    {
        $this->stripe = new StripeClient(config('stripe.local.secret_key'));
    }

    public function getCustomer($customer_id) {
        $customer = $this->stripe->customers->retrieve(
            $customer_id,
            []
        );
        return $customer;
    }

    public function getAllCustomers() {
        $customers= $this->stripe->customers->all(['limit' => 30]);
        return $customers;
    }

    public function createCustomer() {
        $customer= $this->stripe->customers->create();
        return $customer;
    }
}
