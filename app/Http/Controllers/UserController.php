<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //
    public function index() {
        $all_users = User::orderby('created_at', 'desc')->paginate(10);
        return view('admin.user.view')->with([
            'data' => $all_users,
            'page' => $all_users->currentPage(),
            'perPage' => $all_users->perPage(),
        ]);
    }
}
