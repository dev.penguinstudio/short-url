<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\ApiLogger;
use App\Models\ApiModel;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BaseController extends Controller
{
    //
    public $api_model, $user, $final_request, $currentError, $requestId;

    public function __construct(Request $request)
    {
        $this->requestId = implode(
            '-', 
            [
                Str::lower(Str::random(5)),
                Str::lower(Str::random(5)),
                Str::lower(Str::random(5)),
                Str::lower(Str::random(5)),
                Str::lower(Str::random(5)),
            ]
        );
        $this->currentError = null;
        $this->user = null;
        $this->validateAndPrefill($request);
    }

    private function validateAndPrefill(Request $request) {
        $result = $this->getApiModel($request->api_key);
        if($result['code'] != 200) {
            $this->api_model = null;
            $this->currentError = $result;
            return;
        }
        $this->api_model = $result['data'];
        $this->logApi($request);
        $request->request->add([
            'request_id' => $this->requestId
        ]);
        $this->final_request = $request;
        $this->user = $this->api_model->user;
    }

    private function getApiModel($api_key) {
        if(is_null($api_key)) {
            return [
                'code' => 404,
                'message' => APIErrorMessage::$NO_API_KEY
            ];
        }
        $current_api_model = ApiModel::where('api_key', $api_key)->first();
        if(
            $current_api_model && 
            $current_api_model->is_active &&
            $current_api_model->user->status == 1
        ) {
            return [
                'code' => 200,
                'data' => $current_api_model
            ];
        } else {
            return [
                'code' => 422,
                'message' => APIErrorMessage::$API_KEY_INACTIVE
            ];
        }
    }

    private function logApi(Request $request) {
        if(is_null($this->api_model)) {
            return;
        }
        $path = $request->getPathInfo();
        $endPoint = $request->getPathInfo();
        $method = $request->getMethod();
        $api_key = $this->api_model->api_key;
        $user = $this->api_model->user;
        $uri = $request->getRequestUri();

        $api_data = new ApiLogger();
        $api_data->request_id = $this->requestId;
        $api_data->api_key = $api_key;
        $api_data->user_id = $user->uuid;
        $api_data->method = $method;
        $api_data->endpoint = $endPoint;
        $api_data->path = $path;
        $api_data->uri = $uri;
        $api_data->save();
    }

    public function sendResponse(
        string $requestId,
        int $code,
        string $message,
        mixed $data = null
    ) {
        $api_logger = ApiLogger::where('request_id', $requestId)->first();
        if($api_logger) {
            $api_logger->status = $code;
            $api_logger->message = $message;
            $api_logger->save();
        }

        $final_reponse = array(
            'code' => $code,
            'message' => $message
        );

        if($data) {
            $final_reponse['data'] = $data;
        }

        return response()->json(
            $final_reponse,
            $code
        );
    }

    public function logResponse($data) {
        return response()->json(
            $data
        );
    }
}
