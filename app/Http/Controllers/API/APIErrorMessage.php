<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Str;

class APIErrorMessage {
    public static $NO_API_KEY = "API key not found.";
    public static $API_KEY_INACTIVE = "API key not active.";
}