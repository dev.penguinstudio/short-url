<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\BasePath;
use App\Models\ShortUrl;
use Illuminate\Support\Facades\Validator;

class ManageURLController extends BaseController
{
    public function index()
    {
        if($this->currentError) {
            return $this->sendResponse(
                requestId: $this->requestId,
                code: $this->currentError['code'],
                message: $this->currentError['message'],
                data: null
            );
        }
        $method_name = $this->final_request->getMethod();
        switch($method_name) {
            case 'GET':
                return $this->getAllShortURLS();
            case 'POST':
                return $this->store();
            case 'PATCH':
                return $this->update();
            case 'DELETE':
                return $this->destroy();
        }
        return $this->sendResponse(
            requestId: $this->requestId,
            code: 423,
            message: 'Unable to respond you. Please follow api documentation.',
            data: null
        );
    }

    private function getAllShortURLS() {
        $user_id = $this->user->uuid;
        $order_by_clause = $this->getOrderBy();
        if($order_by_clause && count($order_by_clause) > 0) {
            $all_short_url = ShortUrl::
                where('user_id', $user_id)
                ->orderBy($order_by_clause['key'], $order_by_clause['value'])
                ->paginate($this->final_request->per_page ?? 10);
        } else {
            $all_short_url = ShortUrl::
                where('user_id', $user_id)
                ->paginate($this->final_request->per_page ?? 10);
        }
        
        return $this->sendResponse(
            requestId: $this->requestId,
            code: 200,
            message: 'success',
            data: [
                'page_data' => $all_short_url->items(),
                'current_page' => $all_short_url->currentPage(),
                'per_page' => $all_short_url->perPage(),
                'last_page' => $all_short_url->lastPage(),
                'total_items' => $all_short_url->total(),
            ]
        );
    }

    private function getOrderBy() {
        $order_by_clause = $this->final_request->sort_by ?? null;
        if(!$order_by_clause) {
            return null;
        }
        $final_order_by = [];
        switch($order_by_clause) {
            case 'newest_first':
                $final_order_by['key'] = 'created_at';
                $final_order_by['value'] = 'desc';
                break;
        }
        return $final_order_by;
    }

    public function store() {
        $user_id = $this->user->uuid;
        $validateData = Validator::make($this->final_request->all(), [
            'full_url' => 'required|string',
            'short_url' => 'nullable|string|max:10',
            'fallback_url' => 'nullable|active_url'
        ]);

        if($validateData->fails()) {
            return $this->sendResponse(
                requestId: $this->requestId,
                code: 422,
                message: $validateData->errors()->first(),
                data: null
            );
        }

        $short_url_count = count(ShortUrl::where([
            'full_url' => $this->final_request->full_url,
            'user_id' => $user_id
        ])->get());
        if($short_url_count > 0) {
            return $this->sendResponse(
                requestId: $this->requestId,
                code: 422,
                message: "This URL is already exists. You can not take this URL once again.",
                data: null
            );
        }

        $base_path = BasePath::where('user_id', $user_id)->first();
        $short_url = $this->final_request->short_url ?? uniqid();
        $new_short_url = new ShortUrl();
        $new_short_url->uuid = uniqid('surl-' . (int)time() . '-');
        $new_short_url->short_url = ($base_path->base_path ?? uniqid()) .'/'. $short_url;
        $new_short_url->full_url = $this->final_request->full_url;
        $new_short_url->fallback_url = $this->final_request->fallback_url;
        // $new_short_url->domain = $request->domain ?? url();
        $new_short_url->user_id = $user_id;
        $new_short_url->save();
        return $this->sendResponse(
            requestId: $this->requestId,
            code: 201,
            message: "success",
            data: $new_short_url
        );
    }

    public function update() {
        return $this->sendResponse(
            requestId: $this->requestId,
            code: 426,
            message: 'We will come up with this feature soon.',
            data: null
        );
        $user_id = $this->user->uuid;
        $validateData = Validator::make($this->final_request->all(), [
            'uuid' => 'required|string|exists:short_urls,uuid',
            'full_url' => 'required|string|exists:short_urls,full_url',
            'short_url' => 'nullable|string|max:10',
            'fallback_url' => 'nullable|string'
        ]);

        if($validateData->fails()) {
            return $this->sendResponse(
                requestId: $this->requestId,
                code: 422,
                message: $validateData->errors()->first(),
                data: null
            );
        }

        $new_short_url = ShortUrl::where([
            'uuid' => $this->final_request->uuid,
            'full_url' => $this->final_request->full_url,
            'user_id' => $user_id
        ])->first();

        if(!$new_short_url) {
            return $this->sendResponse(
                requestId: $this->requestId,
                code: 422,
                message: "Unable to find out the short url.",
                data: null
            );
        }

        $base_path = BasePath::where('user_id', $user_id)->first();
        $short_url = $this->final_request->short_url ?? uniqid();
        $new_short_url->short_url = ($base_path->base_path ?? uniqid()) .'/'. $short_url;
        $new_short_url->fallback_url = $this->final_request->fallback_url;
        // $new_short_url->domain = $request->domain ?? url();
        $new_short_url->save();
        return $this->sendResponse(
            requestId: $this->requestId,
            code: 200,
            message: "success",
            data: $new_short_url
        );
    }

    private function destroy() {
        return $this->sendResponse(
            requestId: $this->requestId,
            code: 426,
            message: 'We will come up with this feature soon.',
            data: null
        );
        $user_id = $this->user->uuid;
        $validateData = Validator::make($this->final_request->all(), [
            'uuid' => 'required|string|exists:short_urls,uuid'
        ]);

        if($validateData->fails()) {
            return $this->sendResponse(
                requestId: $this->requestId,
                code: 422,
                message: $validateData->errors()->first(),
                data: null
            );
        }

        $new_short_url = ShortUrl::where([
            'uuid' => $this->final_request->uuid,
            'user_id' => $user_id
        ])->first();

        $new_short_url->delete();
        return $this->sendResponse(
            requestId: $this->requestId,
            code: 200,
            message: 'success',
            data: null
        );
    }

    public function details(string $uuid) {
        $user_id = $this->user->uuid;
        $new_short_url = ShortUrl::where([
            'uuid' => $this->final_request->uuid,
            'user_id' => $user_id
        ])->first();
        if(!$new_short_url) {
            return $this->sendResponse(
                requestId: $this->requestId,
                code: 422,
                message: 'Invalid uuid.',
                data: null
            );
        }
        return $this->sendResponse(
            requestId: $this->requestId,
            code: 200,
            message: 'success',
            data: $new_short_url
        );
    }
}
