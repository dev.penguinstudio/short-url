<?php

namespace App\Http\Controllers;

use App\Models\ApiLogger;
use Illuminate\Http\Request;

class ApiLoggerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request)
    {
        //
        $api_log_data = ApiLogger::where('user_id', auth()->user()->uuid)
            ->orderBy('created_at', 'desc')->paginate(10);
        return view('admin.api.api-log')->with([
            'data' => $api_log_data,
            'page' => $api_log_data->currentPage(),
            'perPage' => $api_log_data->perPage()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ApiLogger $apiLogger)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, ApiLogger $apiLogger)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ApiLogger $apiLogger)
    {
        //
    }
}
