<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    //
    public function index(Request $request) {
        return view('admin.subscription.view');
    }
}
