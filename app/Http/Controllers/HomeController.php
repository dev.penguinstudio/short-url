<?php

namespace App\Http\Controllers;

use App\Models\ApiLogger;
use App\Models\ShortUrl;
use App\Models\TrackClick;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $track_redirects = TrackClick::where([
            'user_id' => auth()->user()->uuid,
            'is_fallback' => 0
        ])->count();
        $api_requests = ApiLogger::where('user_id', auth()->user()->uuid)->count();
        $links = ShortUrl::where([
            'user_id' => auth()->user()->uuid,
            'status' => 1
        ])->count();
        $track_clicks = TrackClick::where([
            'user_id' => auth()->user()->uuid
        ])->count();

        $last_five_clicks = TrackClick::where('user_id', auth()->user()->uuid)->orderBy('updated_at', 'desc')->paginate(5);
        // dd($last_five_clicks);
        return view('home')->with([
            'data' => [
                'redirects' => $track_redirects,
                'api_logs' => $api_requests,
                'links'=> $links,
                'clicks' => $track_clicks,
                'last_five_clicks' => $last_five_clicks 
            ],
            // 'last_five_clicks' => $last_five_clicks->items()
        ]);
    }
}
