<?php

namespace App\Http\Controllers;

use App\Models\ApiModel;
use Illuminate\Http\Request;

class ApiModelController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $api_data = ApiModel::where('user_id', auth()->user()->uuid)
            ->orderBy('created_at', 'desc')->get();
        return view('admin.api.view')->with([
            'data' => $api_data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $api_model = new ApiModel();
        $api_model->user_id = auth()->user()->uuid;
        $api_model->api_key = md5(auth()->user()->uuid . time());
        $api_model->save();
        return redirect()->back()->with('success', 'API key has been created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(ApiModel $apiModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ApiModel $apiModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, ApiModel $apiModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ApiModel $apiModel)
    {
        //
    }
}
