<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\ApiModel;
use App\Models\BasePath;
use App\Models\Otp;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        $country =( env('APP_ENV') == 'local') ? file_get_contents('https://ipinfo.io/country?token=0e2f6fc232aa48'): $_SERVER["HTTP_CF_IPCOUNTRY"];
        return view('layouts.auth-v1')->with([
            'country' => trim($country)
        ]);
    }

    public function requestOTP(Request $request) {
        $request->validate([
            'mobile_number' => 'required|numeric|digits_between:10,15'
        ]);
        $otp = Otp::where('mobile_number', $request->mobile_number)->first();
        if($otp) {
            $otp->resend_count = $otp->resend_count + 1;
            if($otp->resend_count > 5) {
                return redirect()->back()->withErrors('Sorry! You have reached maximum retry count. Please try 4hours later.');
            }
        } else {
            $otp = new Otp();
            $otp->uuid = uniqid('otp-' . time() . '-');
            $otp->mobile_number = $request->mobile_number;
            $otp->dial_code = $request->dial_code;
        }
        $otp->otp = (env('APP_DEBUG')) ? 280896 : mt_rand(100000, 999999);
        $otp->expired_at = date('Y-m-d H:i:s',time());
        $otp->save();
        // Sent OTP to real mobile number
        if(env('APP_DEBUG') == false) {
            $this->sendOTP($otp->mobile_number, $otp->otp);
        }
        return redirect()->back()->with([
            'success'=> 'OTP has been sent to '. $request->mobile_number .' successfully.',
            'mobile_number' => $request->mobile_number
        ]);
    }

    public function login(Request $request)
    {
        $request->validate([
            'mobile_number' => 'required|numeric|digits_between:10,15|exists:otps,mobile_number',
            'otp' => 'required|numeric|digits:6|exists:otps,otp',
            'dial_code' => 'required|string',
            'country_code' => 'required|string'
        ]);

        $otp = Otp::where([
            'mobile_number' => $request->mobile_number,
            'otp' => $request->otp
        ])->first();
        if($otp) {
            $otp->delete();
        } else {
            return redirect()->back()->withErrors('Sorry!! Unable to verify you. Please try again later.');
        }
        $user = User::where('mobile_number', $request->mobile_number)->first();
        if(!$user) {
            $user = new User();
            $user->uuid = uniqid('fp-user-' . time() . '-');
            $user->first_name = "Mate";
            $user->country_code = $request->country_code ?? "" ;
            $user->dial_code = $request->dial_code ?? "" ;;
            
            $user->mobile_number = $request->mobile_number;
            $random_password = 'fp-pwd-' . str_pad(mt_rand(1,999_999), 6,'0');
            $user->password = Hash::make($random_password);
            $user->original_password = encrypt($random_password);
            $user->save();

            // Set BasePath here
            $base_path = new BasePath();
            $base_path->user_id = $user->uuid;
            $base_path->base_path = \Str::random(5);
            $base_path->save();

            $api_model = new ApiModel();
            $api_model->user_id = $user->uuid;
            $api_model->api_key = md5($user->uuid);
            $api_model->save();
        }
        $user_login = Auth::loginUsingId($user->id, true);
        if ($user_login) {
            return redirect()->route('home')->with('success', 'Authenticated successfully. Will redirect you to dashboard');
        } else {
            return redirect()->back()->withErrors('Something went wrong. Please try again');
        }
    }

    private function sendOTP($mobile_number, $otp) {
        $apiURL = 'https://fastpigeon.in/api/v1/sendSMS';
        $input_data = [
            'authKey' => '031b074272e981e58e5b2aa6fb670ec0',
            'authSalt' => 'ff6deaf81008bc7b7f7636a87cd087ea',
            'route' => 'O',
            'msg_type' => 'T',
            'sender_id' => 'FSTPGN',
            'template_id' => '1707169011763177982',
            'mobile_number' => $mobile_number,
            'content' => 'Dear Customer, '.$otp.' is your one time password for authentication. Do not share it with anyone. Powered by Fast Pigeon',
        ];
        Http::post($apiURL, $input_data);
    }
}
