<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class IpDetails extends Model
{
    use HasFactory;
    protected $appends = [
        'differenceTillDate',
        'full_region'
    ];

    public function getDifferenceTillDateAttribute() {
        return Carbon::now()->diffInDays($this->updated_at);
    }

    public function getFullRegionAttribute() {
        return implode(', ', [
            $this->city,
            $this->region_name,
            $this->country
        ]);
    }
}
