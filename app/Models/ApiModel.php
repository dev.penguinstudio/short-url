<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApiModel extends Model
{
    use HasFactory;
    protected $append = [
        'user'
    ];

    public function getUserAttribute() {
        $user = User::where('uuid', $this->user_id)->first();
        return $user;
    }
}
