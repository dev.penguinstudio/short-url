<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrackClick extends Model
{
    use HasFactory;

    protected $appends = [
        'ip_details',
        'device_details'
    ];

    public function getIpDetailsAttribute() {
        $ip_details = IpDetails::where('ip', $this->ip)->first();
        if($ip_details) {
            return $ip_details;
        } else {
            return null;
        }
    }

    public function getDeviceDetailsAttribute() {
        $device_details = UserAgent::where([
            'user_id' => $this->user_id,
            'short_url' => $this->short_url,
            'track_id' => $this->uuid
        ])->first();
        if($device_details) {
            return $device_details;
        } else {
            return null;
        }
    }
}
