<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $appends = [
        'is_admin',
        'user_name',
        'role_name'
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'original_password'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'original_password',
        'password',
        'original_password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function getIsAdminAttribute() {
        $filterBy = $this->role;
        $all_roles = config('role');
        $selected_value = array_filter($all_roles, function ($element) use ($filterBy) {
            return ($element['id'] == $filterBy);
        });
        if(count($selected_value) == 0 || count($selected_value) > 1) {
            return false;
        }
        $selected_value = reset($selected_value);
        if($selected_value['name'] == "Admin") {
            return true;
        } else {
            return false;
        }
    }

    public function getUserNameAttribute() {
        return implode(' ', [
            $this->first_name,
            $this->last_name
        ]);
    }

    public function getRoleNameAttribute() {
        $filterBy = $this->role;
        $all_roles = config('role');
        $selected_value = array_filter($all_roles, function ($element) use ($filterBy) {
            return ($element['id'] == $filterBy);
        });
        if(count($selected_value) == 0 || count($selected_value) > 1) {
            return 'User';
        }
        $selected_value = reset($selected_value);
        return $selected_value['name'];
    }
}
