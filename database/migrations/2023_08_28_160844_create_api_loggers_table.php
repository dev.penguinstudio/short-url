<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('api_loggers', function (Blueprint $table) {
            $table->id();
            $table->string('request_id');
            $table->string('user_id');
            $table->string('api_key');
            $table->string('method');
            $table->longText('endpoint');
            $table->longText('path')->nullable();
            $table->longText('uri')->nullable();
            $table->unsignedInteger('status')->nullable();
            $table->longText('message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('api_loggers');
    }
};
